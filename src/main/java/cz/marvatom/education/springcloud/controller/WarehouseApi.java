package cz.marvatom.education.springcloud.controller;

import cz.marvatom.education.springcloud.model.WarehouseDto;
import cz.marvatom.education.springcloud.model.web.WarehouseWithItemWebDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface WarehouseApi {
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WarehouseWithItemWebDto> createItem(@RequestBody WarehouseDto newItem);

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<WarehouseWithItemWebDto> getAll();

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WarehouseWithItemWebDto> getItemById(@PathVariable int id);

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WarehouseDto> updateItem(@PathVariable int id, @RequestBody WarehouseDto newValue);

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WarehouseDto> deleteItemById(@PathVariable int id);
}
