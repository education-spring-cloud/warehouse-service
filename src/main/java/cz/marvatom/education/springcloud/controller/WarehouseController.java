package cz.marvatom.education.springcloud.controller;

import cz.marvatom.education.springcloud.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.mapper.WarehouseMapper;
import cz.marvatom.education.springcloud.model.WarehouseDto;
import cz.marvatom.education.springcloud.model.web.WarehouseWithItemWebDto;
import cz.marvatom.education.springcloud.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WarehouseController implements WarehouseApi {

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private WarehouseMapper mapper;

    @Override
    public ResponseEntity<WarehouseWithItemWebDto> createItem(WarehouseDto newItem) throws EntityNotFoundException {
        WarehouseDto item = warehouseService.createNewItem(newItem);
        return new ResponseEntity<>(mapper.warehouseToWarehouseWithItemWebDto(newItem), HttpStatus.CREATED);
    }

    @Override
    public List<WarehouseWithItemWebDto> getAll() {
        return mapper.warehousesToWarehousesWithItemWebDto(warehouseService.getAllItems());
    }

    @Override
    public ResponseEntity<WarehouseWithItemWebDto> getItemById(int id) {
        return warehouseService.getItemById(id)
                .map(mapper::warehouseToWarehouseWithItemWebDto)
                .map((w) -> new ResponseEntity<>(w, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<WarehouseDto> updateItem(int id, WarehouseDto newValue) {
        return new ResponseEntity<>(warehouseService.updateItem(id, newValue), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<WarehouseDto> deleteItemById(int id) {
        return new ResponseEntity<>(warehouseService.deleteItem(id), HttpStatus.OK);
    }
}
