package cz.marvatom.education.springcloud.mapper;

import cz.marvatom.education.springcloud.model.WarehouseDto;
import cz.marvatom.education.springcloud.model.web.WarehouseWithItemWebDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(WarehouseMapperDecorator.class)
public interface WarehouseMapper {
    WarehouseWithItemWebDto warehouseToWarehouseWithItemWebDto(WarehouseDto dto);
    List<WarehouseWithItemWebDto> warehousesToWarehousesWithItemWebDto(List<WarehouseDto> dto);
}
