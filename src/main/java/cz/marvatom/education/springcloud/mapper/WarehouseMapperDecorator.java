package cz.marvatom.education.springcloud.mapper;


import cz.marvatom.education.springcloud.client.BookClient;
import cz.marvatom.education.springcloud.model.WarehouseDto;
import cz.marvatom.education.springcloud.model.web.BookWebDto;
import cz.marvatom.education.springcloud.model.web.WarehouseWithItemWebDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;

public abstract class WarehouseMapperDecorator implements WarehouseMapper{
    @Autowired
    @Qualifier("delegate")
    private WarehouseMapper delegate;

    @Autowired
    private BookClient identClient;

    @Override
    public WarehouseWithItemWebDto warehouseToWarehouseWithItemWebDto(WarehouseDto dto) {
        BookWebDto bookById = identClient.getById(dto.getBookId());

        WarehouseWithItemWebDto warehouseWithItem = delegate.warehouseToWarehouseWithItemWebDto(dto);
        warehouseWithItem.setBook(bookById);
        return warehouseWithItem;
    }

    @Override
    public List<WarehouseWithItemWebDto> warehousesToWarehousesWithItemWebDto(List<WarehouseDto> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<WarehouseWithItemWebDto> list = new ArrayList<>( dtos.size() );
        for ( WarehouseDto w : dtos ) {
            list.add( warehouseToWarehouseWithItemWebDto( w ) );
        }

        return list;
    }
}
