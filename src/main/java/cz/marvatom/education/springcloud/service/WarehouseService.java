package cz.marvatom.education.springcloud.service;

import cz.marvatom.education.springcloud.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.model.WarehouseDto;

import java.util.List;
import java.util.Optional;

public interface WarehouseService {
    WarehouseDto createNewItem(WarehouseDto newItem);
    List<WarehouseDto> getAllItems();
    Optional<WarehouseDto> getItemById(int id);
    WarehouseDto updateItem(int id, WarehouseDto newVal) throws EntityNotFoundException;
    WarehouseDto deleteItem(int id) throws EntityNotFoundException;

    void orderBooksOutOfStock();
}
