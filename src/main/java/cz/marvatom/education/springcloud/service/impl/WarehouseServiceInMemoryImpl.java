package cz.marvatom.education.springcloud.service.impl;

import cz.marvatom.education.springcloud.client.BookClient;
import cz.marvatom.education.springcloud.client.ProvisioningClient;
import cz.marvatom.education.springcloud.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.model.WarehouseDto;
import cz.marvatom.education.springcloud.model.web.ProvisioningRequestWebDto;
import cz.marvatom.education.springcloud.service.WarehouseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class WarehouseServiceInMemoryImpl implements WarehouseService {

    private static final Logger LOG = LoggerFactory.getLogger(WarehouseServiceInMemoryImpl.class);

    AtomicInteger idGen = new AtomicInteger(1);
    private List<WarehouseDto> items = Collections.synchronizedList(new ArrayList<>());

    @Autowired
    BookClient bookClient;

    @Autowired
    ProvisioningClient provisioningClient;

    @Override
    public WarehouseDto createNewItem(WarehouseDto newItem) {
        bookClient.getById(newItem.getBookId()); //this throws NotFoundException when book doesn't exist

        newItem.setId(idGen.getAndAdd(1));
        synchronized (items) {
            items.add(newItem);
        }
        return newItem;
    }

    @Override
    public List<WarehouseDto> getAllItems() {
        return items;
    }

    @Override
    public Optional<WarehouseDto> getItemById(int id) {
        synchronized (items) {
            return items.stream().filter((i) -> i.getId() == id).findAny();
        }
    }

    @Override
    public WarehouseDto updateItem(int id, WarehouseDto newVal) throws EntityNotFoundException {
        WarehouseDto oldItem = deleteItem(id);
        newVal.setId(oldItem.getId());
        synchronized (items) {
            items.add(newVal);
        }
        return newVal;
    }

    @Override
    public WarehouseDto deleteItem(int id) throws EntityNotFoundException {
        synchronized (items) {
            Iterator<WarehouseDto> iterator = items.listIterator();
            while (iterator.hasNext()) {
                WarehouseDto item = iterator.next();
                if (item.getId() == id) {
                    iterator.remove();
                    return item;
                }
            }
        }
        throw new EntityNotFoundException(String.format("Warehouse item with ID %d doesn't exist.", id));
    }

    @Scheduled(cron = "${cron.order-out-of-stock-books:* * * * * ?}")
    @Override
    public void orderBooksOutOfStock() {
        LOG.info("Finding books out of stock.");
        synchronized (items){
            items.stream()
                    .filter((i) -> i.getPcsOnStock() == 0)
                    .forEach((i) -> orderNewBooks(i));
        }
    }

    private void orderNewBooks(WarehouseDto item) {
        LOG.info(String.format("Order book with ID %d", item.getBookId()));
        ProvisioningRequestWebDto provisioningRequestWebDto = provisioningClient.orderBook(item.getBookId());
        item.setLastProvisioningTime(provisioningRequestWebDto.getCreated());
        item.setProvisionedPcs(provisioningRequestWebDto.getNumOfPsc());
        item.setPcsOnStock(provisioningRequestWebDto.getNumOfPsc());
    }
}
