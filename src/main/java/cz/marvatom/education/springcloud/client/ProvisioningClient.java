package cz.marvatom.education.springcloud.client;

import cz.marvatom.education.springcloud.model.web.ProvisioningRequestWebDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
public class ProvisioningClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${api.provisioning.base-url}")
    private String provisioningServiceBaseURL;

    public ProvisioningRequestWebDto orderBook(int bookId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<?> request = new HttpEntity(headers);
        ResponseEntity<ProvisioningRequestWebDto> entity = restTemplate.exchange(provisioningServiceBaseURL + "/{bookId}",
                HttpMethod.POST, request, ProvisioningRequestWebDto.class, bookId);
        return entity.getBody();
    }
}
