package cz.marvatom.education.springcloud.client;

import cz.marvatom.education.springcloud.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.model.web.BookWebDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class BookClient {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${api.book.base-url}")
    private String bookServiceBaseURL;

    public BookWebDto getById(int id) {
        try {
            return restTemplate.getForObject(bookServiceBaseURL + "/{id}", BookWebDto.class, id);
        } catch (HttpClientErrorException e){
            if (e.getRawStatusCode() == 404){
                throw new EntityNotFoundException(String.format("Book service responds with 404. Unknown id %d", id), e);
            }
            throw e;
        }
    }

}
