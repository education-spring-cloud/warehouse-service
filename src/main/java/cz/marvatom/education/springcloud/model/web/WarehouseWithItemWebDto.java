package cz.marvatom.education.springcloud.model.web;

import java.time.LocalDateTime;

public class WarehouseWithItemWebDto {
    private int id;
    private BookWebDto book;
    private int pcsOnStock;
    private LocalDateTime lastProvisioningTime;
    private int provisionedPcs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BookWebDto getBook() {
        return book;
    }

    public void setBook(BookWebDto book) {
        this.book = book;
    }

    public int getPcsOnStock() {
        return pcsOnStock;
    }

    public void setPcsOnStock(int pcsOnStock) {
        this.pcsOnStock = pcsOnStock;
    }

    public LocalDateTime getLastProvisioningTime() {
        return lastProvisioningTime;
    }

    public void setLastProvisioningTime(LocalDateTime lastProvisioningTime) {
        this.lastProvisioningTime = lastProvisioningTime;
    }

    public int getProvisionedPcs() {
        return provisionedPcs;
    }

    public void setProvisionedPcs(int provisionedPcs) {
        this.provisionedPcs = provisionedPcs;
    }
}
