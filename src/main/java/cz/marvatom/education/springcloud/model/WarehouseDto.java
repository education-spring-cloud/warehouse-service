package cz.marvatom.education.springcloud.model;

import java.time.LocalDateTime;

public class WarehouseDto {
    private int id;
    private int bookId;
    private int pcsOnStock;
    private LocalDateTime lastProvisioningTime;
    private int provisionedPcs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getPcsOnStock() {
        return pcsOnStock;
    }

    public void setPcsOnStock(int pcsOnStock) {
        this.pcsOnStock = pcsOnStock;
    }

    public LocalDateTime getLastProvisioningTime() {
        return lastProvisioningTime;
    }

    public void setLastProvisioningTime(LocalDateTime lastProvisioningTime) {
        this.lastProvisioningTime = lastProvisioningTime;
    }

    public int getProvisionedPcs() {
        return provisionedPcs;
    }

    public void setProvisionedPcs(int provisionedPcs) {
        this.provisionedPcs = provisionedPcs;
    }
}
