package cz.marvatom.education.springcloud.model.web;

import java.time.LocalDateTime;

public class ProvisioningRequestWebDto {
    private int bookId;
    private LocalDateTime created;
    private int numOfPsc;

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public int getNumOfPsc() {
        return numOfPsc;
    }

    public void setNumOfPsc(int numOfPsc) {
        this.numOfPsc = numOfPsc;
    }
}
