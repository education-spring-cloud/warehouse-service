# Warehouse service
This service manages book warehouse. 

## Changelog
### Version 1.0
This version contains CRUD REST API. It's fully standalone and uses no other services.  

### Version 1.1
Communication with book service and scheduled job which contacts provisioning service for books out of stock. 

## Docker 
To build image from this directory:
```
docker build --tag gopas/warehouse-service .
```
And run it:
```
docker run -d --name warehouse-service -p9090:9090 gopas/warehouse-service
```
Stop it:
```
docker stop warehouse-service 
```
Remove container:
```
docker rm warehouse-service
```